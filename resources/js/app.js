var jQuery = window.$ = window.jQuery = require('jquery/dist/jquery');
var bootstrap = require('bootstrap');
var Highcharts = require('./Highcharts/code/highcharts');

var loadData = function (type, callback) {
    $.get('/api/data/' + type, function (data) {
        callback(data);
    });
};

var loadWeeklyChart = function () {
    $.getJSON('/api/users', function (data) {
        Highcharts.chart('weekly-curve', {
            chart: {
                type: 'spline'
            },
            
            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },

            yAxis: {
                title: {
                    text: 'Onboarding User Percentage %'
                }
            },
            xAxis: {
                title: {
                    text: 'Onboarding Percentage'
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'middle'
            },

            series: data
        });
    });
};
var loadUpscaleChart = function () {

};
var loadChart = function (chart) {
    switch (chart) {
        case 'weekly':
            loadWeeklyChart();
        case 'upcase':
            loadUpscaleChart();
        default:
            loadWeeklyChart();
    }
};
$(function () {
    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        loadChart(e.currentTarget.hash);
    });
    loadChart('weekly');
});
