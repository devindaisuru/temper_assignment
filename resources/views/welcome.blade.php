<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Temper Assignment</title>
    </head>
    <body>
        <div class="container">
            <h1>WEEKLY RETENTION CURVE</h1>
            <div id="weekly">
                <div id="weekly-curve"></div>
            </div>
        </div>
        
        <link href="/css/app.css" rel="stylesheet"/>

        <!-- Latest compiled and minified JavaScript -->
        <script src="/js/app.js" ></script>
    </body>
</html>
