# temper_assignment

installation instructions:
1. git clone https://devindaisuru@bitbucket.org/devindaisuru/temper_assignment.git
2. composer install
3. npm install
4. from the root folder execute php artisan migrate:refersh --seed
5. from the root folder execute php artisan serve
(note: I have developed on php 7.2)
6. from the root folder execute npm run watch in another console window
7. Application should run now and see the required output
8. To run feature test from the root folder run ./vendor/bin/phpunit tests/Feature/UserTest.php
9. To run unit test from root folder run ./vendor/bin/phpunit tests/Unit/UserTest.php
8. Attached the screenshots.
 
