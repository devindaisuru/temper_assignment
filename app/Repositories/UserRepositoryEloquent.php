<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\Entities\User;
use App\Validators\UserValidator;
use Carbon\Carbon;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
    
    /*
     * Retrives the formatted data
     */
    public function getData(){
        $data = $this->model
                ->select(\DB::raw('DATE(SUBDATE(`created_at`, weekday(`created_at`))) as `week`'),\DB::raw('count(`onboarding_perentage`) as `percent`'), 'onboarding_perentage', 'created_at')
                ->groupBy('week')
                ->groupBy('onboarding_perentage')
                ->groupBy('created_at')
                ->get();
                
        return $data;
    }
    
    /*
     * Format the filtered data
     */
    public function formatData($data){
        $response = [];
        
        $weekdata = [];
        
        // order the data by week
        foreach($data as $entry){
            if(!isset($weekdata[$entry->week])){
                $weekdata[$entry->week] = [];
            }
            if(!isset($weekdata[$entry->week][$entry->onboarding_perentage])){
                $weekdata[$entry->week][$entry->onboarding_perentage] = 0;
            }
            $weekdata[$entry->week][$entry->onboarding_perentage] += $entry->percent;
        }
        
        // calculate percentages
        foreach($weekdata as $week => $dataset){
            $total = array_sum(array_values($dataset));
            
            $dataset = [0 => $total] + $dataset;
            foreach($dataset as $percent => &$datapoint){
                $datapoint = intval(($datapoint/$total)*100);
            }
            $weekdata[$week] = $dataset;
        }
        
        // set highchart data
        foreach($weekdata as $week => $dataset){
            $dataGroups = [];
            
            foreach($dataset as $key => $point){
                $dataGroups[] = [$key, $point];
            }
            $response[] = [
                'name' => $week,
                'data' => $dataGroups
            ];
        }
        return $response;
    }
    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
