<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class UsersController extends Controller
{
    private $userRepo;
    public function __construct(UserRepository $repository){
        $this->userRepo = $repository;
    }

    /**
     * Return Formatted Data
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rawData = $this->userRepo->getData();
        $dataFormatted = $this->userRepo->formatData($rawData);
        return response($dataFormatted, 200);
    }
}
