<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\UserRepository;

class UserTest extends TestCase
{
    /**
     * test testGetData in UserRepositoryEloquent
     *
     * @return void
     */
    public function testGetData()
    {
        $userRepo = \App::make('App\Repositories\UserRepository');
        $data = $userRepo->getData();
        
        // Check whether the output is an array
        $this->assertEquals("Illuminate\Database\Eloquent\Collection",get_class($data));
        
        if("Illuminate\Database\Eloquent\Collection" == get_class($data)){
        
            // Check if single object has expected parameters
            $firstSample = $data->first();
            $this->assertTrue(isset($firstSample->week));
            $this->assertTrue(isset($firstSample->onboarding_perentage));
            $this->assertTrue(isset($firstSample->percent));
        }
    }
    
    /**
     * test testGetData in formatData
     *
     * @return void
     */
    public function testFormatData()
    {
        $userRepo = \App::make('App\Repositories\UserRepository');
        $data = $userRepo->getData();
        
        // get data set of 10 sample data fro 10-19
        $slice = $data->slice(10, 10);
        
        $arrayMust = json_decode('[{"name":"2016-07-18","data":[[0,100],[50,6],[95,6],[99,53],[100,33]]}]', true);
        
        // check the formatted data
        $this->assertEquals($arrayMust,$userRepo->formatData($slice->all()));
    }
}
