<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/api/users');

        $response
        ->assertStatus(200)
        ->assertJson(json_decode('[{"name":"2016-07-18","data":[[0,100],[35,0],[40,22],[45,1],[50,1],[95,0],[99,4],[100,12]]},{"name":"2016-07-25","data":[[0,100],[40,31],[45,0],[50,0],[55,1],[60,1],[65,0],[95,3],[99,7],[100,13]]},{"name":"2016-08-01","data":[[0,100],[35,0],[40,12],[50,0],[55,1],[65,3],[95,4],[99,8],[100,7]]},{"name":"2016-08-08","data":[[0,100],[40,6],[95,2],[99,12],[100,7]]}]', true));
    }
}
