<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = base_path() . '/data/data.csv';
        $users = $this->csv_to_array($csvFile);
        DB::table('users')->insert($users);
    }
    
    function csv_to_array($filename = '', $delimiter = ',') {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if (!$header)
                    $header = $row;
                else{
                    $rowData = explode(";", $row[0]);
                    $data[] = [
                        'id' => $rowData[0],
                        'created_at'            => $rowData[1],
                        'updated_at'            => $rowData[1],
                        'onboarding_perentage'  => (($rowData[2] == '')?0:$rowData[2]),
                    ];
                }
            }
            fclose($handle);
        }
        return $data;
    }

}
